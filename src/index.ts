import express from "express"
import { createServer } from "http";
import { Server } from "socket.io";
const port = process.env.PORT || 3000;
const app = express()
const serverHttp = createServer(app);
const serverSocket = new Server(serverHttp);
app.use(express.static("frontend/js"));
app.use(express.static("frontend/html"));
import * as Sentry from '@sentry/node'
import * as Tracing from "@sentry/tracing"

import { MovePacket, SetupData, AddPlayerPacket, AddFruitPacket } from "../frontend/networkCommon"
import { Game } from "../frontend/game.js";
const game = new Game();
game.state.screen.width = 10;
game.state.screen.height = 10;
Sentry.init({
  dsn: "https://f8c37fa2c37b4077b3296462533c4ebb@o935687.ingest.sentry.io/5885641",
  integrations: [
    new Sentry.Integrations.Http({ tracing: true }),
    new Tracing.Integrations.Express({ app }),
  ],

  tracesSampleRate: 1.0,
});

serverSocket.on("connection", (socket)=>{
  console.log("Connection!");
  socket.on("move", (movePacket: MovePacket)=>{
    let player = game.getPlayer(socket.id);
    player.x = movePacket.pos.x;
    player.y = movePacket.pos.y;
    movePacket.player = socket.id;
    for(let fruitId in game.state.fruits){
      let fruit = game.getFruit(fruitId);

      if(fruit.x === player.x && fruit.y === player.y){
          game.removeFruit(fruitId);
          socket.broadcast.emit("pickup-fruit", fruitId);
      }
  }
    socket.volatile.broadcast.emit("move", movePacket);
  })
  const player = game.addPlayer({id:socket.id})
  const setup: SetupData = {
    player: socket.id,
    state: game.state
  }

  socket.on("disconnect", ()=>{
    game.removePlayer(socket.id);
    socket.broadcast.emit("playerRemove", socket.id);

    console.log("Disconnected!");
  })
  socket.emit("setup", setup);
  const addPlayerPacket: AddPlayerPacket = {
    id: socket.id,
    player
  }
  socket.broadcast.emit("addPlayer", addPlayerPacket);
})
setInterval(()=>{
  let id = Math.floor(Math.random() * 10000000);
  
  let packet: AddFruitPacket = {
    fruit: game.addFruit({id}),
    id
  }
  serverSocket.sockets.emit("add-fruit", packet);
},1000)
app.use(Sentry.Handlers.requestHandler());
app.use(Sentry.Handlers.tracingHandler());
app.use(function onError(err, req, res, next) {
  res.statusCode = 500;
  res.end(res.sentry + "\n");
});
serverHttp.listen(port, function () {
  console.log('Started at http://localhost:'+port);
})

export {serverSocket}