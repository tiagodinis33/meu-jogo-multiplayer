import Renderer from "./renderer.js";
import { context, canvas } from "./renderer.js";
import {Game} from "./game.js"
import {initKeyboard} from "./keyboardListener.js"
import {initSocket} from "./network.js"
const globalGame = new Game();

const globalRenderer = new Renderer(()=>{
    for(const playerId in globalGame.state.players){
        const player = globalGame.getPlayer(playerId);
        context!!.fillStyle = "green";
        context?.fillRect(player.x,player.y, 1, 1);
    }
    for(const fruitId in globalGame.state.fruits){
        const fruit = globalGame.getFruit(fruitId);
        context!!.fillStyle = "yellow";
        context?.fillRect(fruit.x, fruit.y, 1, 1);
    }
}, ()=>{
    canvas.width =globalGame.state.screen.width;
    canvas.height = globalGame.state.screen.height;
})

globalRenderer.gameLoop();
initKeyboard();
initSocket();
export {globalGame}