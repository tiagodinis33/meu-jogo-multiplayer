import { Fruit, Player, State } from "./game";
type PlayerID = string;
class SetupData{
    state?: State
    player?: PlayerID
}
class Position {
    x?: number
    y?: number
}
class MovePacket {
    pos?: Position
    player?: PlayerID
}
class AddPlayerPacket {
    id?: PlayerID
    player?: Player
}
class AddFruitPacket {
  id?: number
  fruit?: Fruit  
}
export {SetupData, MovePacket , Position, AddPlayerPacket, AddFruitPacket};