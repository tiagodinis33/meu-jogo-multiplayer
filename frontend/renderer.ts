const canvas = document.getElementById("tela") as HTMLCanvasElement;
const context = canvas.getContext("2d");

export default class Renderer {
    constructor(public draw: ()=>void, public update: ()=>void){}
    clearScreen() {
        context!!.fillStyle = "white";
        context!!.clearRect(0, 0, 60, 60);
    }
    gameLoop() {
        this.clearScreen();
        this.update();
        this.draw();
        requestAnimationFrame(()=>{
            this.gameLoop();
        });
    }
}
export { canvas, context }