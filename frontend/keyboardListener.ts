import { globalGame } from "./index.js"
import {socket} from "./network.js"
import { MovePacket } from "./networkCommon.js";
function initKeyboard() {
    document.addEventListener("keydown", function (e) {
        globalGame.handleKey(e.key);
        const movePacket: MovePacket = {
            player: globalGame.thePlayerID,
            pos: {
                x: globalGame.thePlayer.x,
                y: globalGame.thePlayer.y
            }
        }
        socket.emit("move", movePacket);
        
    })
}
export {initKeyboard}