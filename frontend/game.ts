class Screen {
    constructor(width: number = 60, height: number = 60) {
        this.width = width;
        this.height = height;
    }
    width: number;
    height: number;
}
class Player {
    constructor(public x: number, public y: number) { }
    
}
class Fruit {
    constructor(public x: number, public y: number) { }
}
class State {
    players: any = {}
    fruits: any = {}
    screen = new Screen()

}
class FruitCommand {
    x?: number;
    y?: number;
    id?: number;
}
class PlayerCommand {
    x?: number;
    y?: number;
    id?: string;
}
class Game {
    state = new State();
    get thePlayer(): Player{
        return this.getPlayer(this.thePlayerID!!);
    }
    thePlayerID?: string;
    removeFruit(id: string){
        delete this.state.fruits[id];
    }
    removePlayer(id: string){
        delete this.state.players[id];
    }
    handleKey(key: string): boolean {
        const self = this;
        const acceptedMoves: any = {
            ArrowUp() {
                self.thePlayer!!.y--;
            },
            ArrowDown() {
                self.thePlayer!!.y++;
            },
            ArrowLeft() {
                self.thePlayer!!.x--;
            },
            ArrowRight() {
                self.thePlayer!!.x++;
            }

        }
        const move = acceptedMoves[key];
        if (!move) {
            return false;
        }
        move();
        for(let fruitId in this.state.fruits){
            let fruit = this.getFruit(fruitId);
      
            if(fruit.x === this.thePlayer.x && fruit.y === this.thePlayer.y){
                this.removeFruit(fruitId);
            }
        }
        return true;
    }
    getFruit(id: string): Fruit {
        return this.state.fruits[id] as Fruit;
    }
    getPlayer(id: string): Player {
        return this.state.players[id] as Player;
    }

    addPlayer(command: PlayerCommand): Player {
        const playerId = command.id ? command.id : Math.floor(Math.random() * 10000000)
        const playerY = command.y ? command.y : Math.floor(Math.random() * this.state.screen.height)
        const playerX = command.x ? command.x : Math.floor(Math.random() * this.state.screen.width)

        this.state.players[playerId] = new Player(playerX, playerY);
        return this.state.players[playerId];
    }

    addFruit(command: FruitCommand): Fruit {
        const fruitId = command.id ? command.id : Math.floor(Math.random() * 10000000)
        const fruitY = command.y ? command.y : Math.floor(Math.random() * this.state.screen.height)
        const fruitX = command.x ? command.x : Math.floor(Math.random() * this.state.screen.width)

        this.state.fruits[fruitId] = new Fruit(fruitX, fruitY);
        return this.state.fruits[fruitId];
    }
}
export { State, Game, Fruit, Player, Screen };
