import { AddFruitPacket, AddPlayerPacket, MovePacket, SetupData } from "./networkCommon.js";
import { globalGame } from "./index.js";
// @ts-ignore
const socket = io();

function initSocket(){
    socket.on("setup", (data: SetupData)=>{
        globalGame.state = data.state!!;
        globalGame.thePlayerID = data.player;
    })
    socket.on("move", (data: MovePacket)=>{
        const player = globalGame.getPlayer(data.player!!);
        player.x = data.pos!!.x!!;
        player.y = data.pos!!.y!!;
        
    })
    socket.on("playerRemove", (data: string)=>{
        globalGame.removePlayer(data);
    })
    socket.on("addPlayer", (data: AddPlayerPacket) =>{
        globalGame.addPlayer({
            id: data.id,
            ...data.player
        })
    });
    socket.on("add-fruit", (data: AddFruitPacket)=>{
        globalGame.addFruit({id:data.id, ...data.fruit});
    })
    socket.on("pickup-fruit", (fruitId: string)=>{
        globalGame.removeFruit(fruitId);
    })
}
export { socket, initSocket }